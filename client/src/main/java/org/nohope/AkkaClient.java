package org.nohope;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.remote.netty.ClassLoadingNettyTransport;

/**
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/7/12 1:37 AM
 */
public class AkkaClient {
    private AkkaClient() {
    }

    public static void main(final String... args) throws InterruptedException {
        final ActorSystem system = AkkaUtils.createRemoteSystem("client",
                ClassLoadingNettyTransport.class.getCanonicalName(),
                5554);

        system.actorOf(new Props(ClassReaderActor.class), "class-reader");

        final ActorRef ref = system.actorFor("akka://server@localhost:5555/user/test");
        ref.tell(new ClientClass(), system.guardian());

        Thread.sleep(Long.MAX_VALUE);
    }
}
