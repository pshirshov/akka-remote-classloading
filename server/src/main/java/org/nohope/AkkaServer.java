package org.nohope;

import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.remote.netty.ClassLoadingNettyTransport;

/**
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/7/12 1:55 AM
 */
public class AkkaServer {
    private AkkaServer() {
    }

    public static void main(final String... args) throws InterruptedException {
        final ActorSystem system = AkkaUtils.createRemoteSystem("server",
                ClassLoadingNettyTransport.class.getCanonicalName(),
                5555);
        system.actorOf(new Props(TestActor.class), "test");

        Thread.sleep(Long.MAX_VALUE);
    }

    /**
     * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
     * @since 10/7/12 1:59 AM
     */
    public static class TestActor extends UntypedActor {
        /**
         * To be implemented by concrete UntypedActor, this defines the behavior of the
         * UntypedActor.
         */
        @Override
        public void onReceive(final Object message) {
            try {
                System.err.println("received -> " + message);
            } catch (final Throwable e) {
                e.printStackTrace();
            }
        }
    }
}
