package org.nohope;

import akka.actor.UntypedActor;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/7/12 1:41 AM
 */
public class ClassReaderActor extends UntypedActor {
    @Override
    public void onReceive(final Object message) {
        System.err.println("req -> " + message);
        if (message instanceof String) {
            try {
                final String name = ((String) message).replaceAll("\\.", "/") + ".class";
                System.err.println("request -> " + name);
                final byte[] msg = IOUtils.toByteArray(ClassLoader.getSystemResource(name));
                System.err.println("result -> " + Arrays.toString(msg));
                getSender().tell(msg, getSender());
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.err.println("bad request -> " + message);
        }

        getSender().tell(null, getSender());
    }
}
