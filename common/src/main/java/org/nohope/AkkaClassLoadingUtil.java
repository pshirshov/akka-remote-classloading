package org.nohope;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.pattern.Patterns;
import akka.util.Timeout;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.Remapper;
import org.objectweb.asm.commons.RemappingClassAdapter;
import scala.concurrent.Future;
import scala.runtime.AbstractPartialFunction;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

/**
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/7/12 6:41 AM
 */
public class AkkaClassLoadingUtil {
    private static final ClassLoader loader = ClassLoader.getSystemClassLoader();
    private static final Method method;

    private AkkaClassLoadingUtil() {
    }

    static {
        try {
            method = ClassLoader.class.getDeclaredMethod(
                    "defineClass", String.class,
                    byte[].class, int.class, int.class);
            method.setAccessible(true);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException(e);
        }
    }

    public static class Collector extends Remapper {

        private final Set<String> classNames;
        private final String prefix;

        public Collector(final Set<String> classNames, final String prefix){
            this.classNames = classNames;
            this.prefix = prefix;
        }

        @Override
        public String mapDesc(final String desc){
            if(desc.startsWith("L")){
                this.addType(desc.substring(1, desc.length() - 1));
            }
            return super.mapDesc(desc);
        }

        @Override
        public String[] mapTypes(final String[] types){
            for(final String type : types){
                this.addType(type);
            }
            return super.mapTypes(types);
        }

        private void addType(final String type){
            final String className = type.replace('/', '.');
            if(className.startsWith(this.prefix)) {
                try {
                    Class.forName(className);
                } catch (ClassNotFoundException e) {
                    this.classNames.add(className);
                }
            }
        }

        @Override
        public String mapType(final String type){
            this.addType(type);
            return type;
        }
    }

    private static Set<String> findClassNames(final byte[] byteCode) {
        final ClassReader reader = new ClassReader(byteCode);
        final Set<String> classes = new HashSet<>();
        final Remapper remapper = new Collector(classes, "");
        final ClassVisitor inner = new ClassWriter(reader, 0);
        final RemappingClassAdapter visitor = new RemappingClassAdapter(inner, remapper);
        reader.accept(visitor, 0);
        return classes;
    }

    private static boolean isClassDefined(final String name) {
        try {
            Class.forName(name);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    private static void defineClass(final String name, final byte[] bytes) {
        try {
            if (!isClassDefined(name)) {
                method.invoke(loader, name, bytes, 0, bytes.length);
            }
        } catch (InvocationTargetException
                | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
    }

    public static void defineClassFrom(
            final ActorRef ref,
            final String className,
            final Runnable future,
            final ActorSystem system) {

        if (ref.isTerminated()) {
            return;
        }

        final Future<Object> childResponse =
                Patterns.ask(ref, className, Timeout.longToTimeout(5000));

        childResponse.onSuccess(new AbstractPartialFunction<Object, Object>() {
            @Override
            public Object apply(final Object x) {
                final byte[] byteCode = (byte[]) x;
                defineClass(className, byteCode);
                final Set<String> classNames = findClassNames(byteCode);
                final CountDownLatch latch = new CountDownLatch(classNames.size());
                for (final String depClassName : classNames) {
                    defineClassFrom(ref, depClassName, new Runnable() {
                        @Override
                        public void run() {
                            latch.countDown();
                        }
                    }, system);
                }

                try {
                    latch.await(); // FIXME: can fail
                } catch (InterruptedException e) {
                    return null;
                }
                future.run();
                return null;
            }

            @Override
            public boolean isDefinedAt(final Object x) {
                return x instanceof byte[];
            }
        }, system.dispatcher());
    }
}
